#include <Arduino.h>

#include <Wire.h>
#include <Adafruit_SPIDevice.h>

// GxEPD2_GFX can be used to pass references or pointers to the display instance; uses ~1.2k more code
//#define ENABLE_GxEPD2_GFX 0
// use class GFX of library GFX_Root instead of Adafruit_GFX
//#include <GFX.h>
#include <GxEPD2_3C.h>

#include <WiFiClient.h>

#include <ESPAsyncWebServer.h>
#include <AsyncJson.h>
#include <SPIFFS.h>

#include <Preferences.h>

#include <ESPmDNS.h>
#include <ArduinoOTA.h>

///////////////////////////////////////////////////////////

#define ELINK_BUSY     4
#define ELINK_RESET    16
#define ELINK_DC       17
#define ELINK_SS       5

#define SPI_MOSI       23
#define SPI_CLK        18

#define SDCARD_SS      13
#define SDCARD_MOSI    15
#define SDCARD_MISO    2
#define SDCARD_CLK     14

#define BUTTON_1       37
#define BUTTON_2       38
#define BUTTON_3       39

#define SPEAKER_OUT    25

#define AMP_POWER_CTRL 19

// Settings defined using platformio_local.ini
#if !defined(HOSTNAME) || !defined(WIFISSID) || !defined(WIFIPASS) || !defined(TIMEZONE)
  #error Missing settings from build configuration
#endif

// Possible Options (296 total pixels):
//   16 (16, 40) -- very dense, but gets lots of rows
//   14 (18, 44) -- nice with rolling hours display
//   12 (21, 44) -- full work day
//    8 (32, 40) -- lots of whitespace
#define HOURS_TO_SHOW 12
#define PXH_PER_HOUR  21
#define PXH_START     44

// comment out to use a rolling hours display
#define DISPLAY_HOUR_START 8

///////////////////////////////////////////////////////////

void connectWifi();
void renderDisplay();
void serverSetup();
void statePersist();
void stateRestore();

///////////////////////////////////////////////////////////

Preferences prefs;

AsyncWebServer server(80);

GxEPD2_3C<GxEPD2_290c, GxEPD2_290c::HEIGHT> display(GxEPD2_290c(ELINK_SS, ELINK_DC, ELINK_RESET, ELINK_BUSY));

enum BusyState {
  BS_FREE,
  BS_BUSY,
  BS_DND,
};

// 7 days, 24 hours per day, 2 half-hours per hour
BusyState schedule[7 * 24 * 2];

// Message (10-chars on one line) or Bitmap
int messageSize = 2;
char messageBuffer[24];

struct tm timeinfo;

long lastRefresh = 0;
int lastHalfhour = -1;
bool dataChanged = false;

///////////////////////////////////////////////////////////

void setup() {
  // Initialize serial debugging
  Serial.begin(115200);
  Serial.println();
  Serial.println("Busy Calendar");

  // Initialize display
  display.init(115200);
  display.setRotation(2); // buttons on top

  // Initialize SPIFFS
  SPIFFS.begin();

  // Set time stuff
  configTzTime(TIMEZONE,
               "time.google.com", "pool.ntp.org");

  // Prepare wifi
  connectWifi();

  // Initialize state
  memset(messageBuffer, 0, sizeof(messageBuffer));
  memset(&schedule, 0, sizeof(schedule));
  stateRestore();

  serverSetup();
}

void loop() {
  ArduinoOTA.handle();
  if (Update.isRunning()) {
    return;
  }

  if (getLocalTime(&timeinfo)) {
    int currHalfhour = timeinfo.tm_hour * 2 + (timeinfo.tm_min < 30 ? 0 : 1);
    if (currHalfhour != lastHalfhour || dataChanged) {
      lastHalfhour = currHalfhour;
      dataChanged = false;
      renderDisplay();
    }
  }

  delay(10);
  yield();
}

///////////////////////////////////////////////////////////

void connectWifi() {
  Serial.println(__DATE__ " " __TIME__);
  Serial.print("Using hostname ");
  Serial.println(HOSTNAME);
  Serial.print("Connecting to ");
  Serial.println(WIFISSID);

  display.println(__DATE__);
  display.println(__TIME__);
  display.println(HOSTNAME);
  display.println(WIFISSID);
  display.display();

  WiFi.persistent(false);
  WiFi.mode(WIFI_STA);
  WiFi.setHostname(HOSTNAME);
  WiFi.begin(WIFISSID, WIFIPASS);

  // Spin here until wifi is connected
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println();
  Serial.println(WiFi.localIP());

  // Prepare for network visibility
  MDNS.begin(HOSTNAME);
  MDNS.addService("http", "tcp", 80);
  MDNS.addService("busyindicator", "tcp", 80);
  MDNS.addService("busycalendar", "tcp", 80);

  // Prepare OTA support
  ArduinoOTA.setHostname(HOSTNAME);
  ArduinoOTA.begin();
}

void serverSetup() {
  // ENDPOINT: static files
  server.serveStatic("/", SPIFFS, "/").setDefaultFile("index.html");

  // ENDPOINT: POST display
  // curl --header "Content-Type: application/json" --data '{"text": "Hi", "size": 2}' http://hostname/api/display
  server.addHandler(new AsyncCallbackJsonWebHandler(
        "/api/display", [](AsyncWebServerRequest *request, JsonVariant &json) {
    int code = 400;

    if (json.isNull()) {
      code = 200;
    }
    else {
      JsonObjectConst jsonObj = json.as<JsonObject>();
      if (!jsonObj.isNull()) {
        messageSize = max(1, min(jsonObj["size"] | 2, 4));
        strlcpy(messageBuffer, jsonObj["text"] | "", sizeof(messageBuffer));
        dataChanged = true;

        code = 200;
      }
    }

    request->send(code);
  }));

  // ENDPOINT: GET display
  // curl http://hostname/api/display
  server.on("/api/display", HTTP_GET, [](AsyncWebServerRequest *request) {
    AsyncResponseStream  *response = request->beginResponseStream("application/json");
    DynamicJsonDocument root(1024);

    root["text"] = messageBuffer;
    root["size"] = messageSize;

    serializeJson(root, *response);
    request->send(response);
  });

  // ENDPOINT: POST schedule
  // curl --header "Content-Type: application/json" --data '[ 0, 1, 2, ... 335 ]' http://hostname/api/schedule
  server.addHandler(new AsyncCallbackJsonWebHandler(
        "/api/schedule", [](AsyncWebServerRequest *request, JsonVariant &json) {
    int code = 400;

    Serial.println("hi");
    if (json.isNull()) {
      code = 200;
    }
    else {
      JsonArrayConst jsonArr = json.as<JsonArray>();
      Serial.println(jsonArr.size());
      if (jsonArr.size() == LWIP_ARRAYSIZE(schedule)) {
        for (size_t ix = 0; ix < LWIP_ARRAYSIZE(schedule); ++ix) {
          schedule[ix] = jsonArr[ix];
        }
        dataChanged = true;
        statePersist();

        code = 200;
      }
    }

    request->send(code);
  }, 8192));

  // ENDPOINT: GET schedule
  // curl http://hostname/api/display
  server.on("/api/schedule", HTTP_GET, [](AsyncWebServerRequest *request) {
    AsyncResponseStream  *response = request->beginResponseStream("application/json");
    DynamicJsonDocument root(8192);

    JsonArray data = root.to<JsonArray>();
    for (size_t ix = 0; ix < LWIP_ARRAYSIZE(schedule); ++ix) {
      data.add(schedule[ix]);
    }

    serializeJson(root, *response);
    request->send(response);
  });

  server.begin();
}

// Remember to bump this if state format changes.
#define STATE_PERSIST_VERSION 1

void statePersist() {
  if (prefs.begin("busycalendar", false)) {
    prefs.clear();

    prefs.putUChar("version", STATE_PERSIST_VERSION);
    prefs.putBytes("schedule", schedule, sizeof(schedule));

    prefs.end();
  }
}

void stateRestore() {
  if (prefs.begin("busycalendar", true)) {
    if (prefs.getUChar("version") == STATE_PERSIST_VERSION) {

      if (prefs.getBytesLength("schedule") == sizeof(schedule)) {
        prefs.getBytes("schedule", schedule, sizeof(schedule));
      }

    }
    prefs.end();
  }
}

int BusyStateToColor(BusyState state) {
    switch (state) {
      case BS_FREE:  return GxEPD_WHITE;
      case BS_BUSY:  return GxEPD_BLACK;
      case BS_DND:   return GxEPD_RED;
    }
    return GxEPD_WHITE;
}

void renderStatus() {
  display.setTextSize(messageSize);
  display.setTextColor(GxEPD_BLACK);
  display.fillRect(0, 0, display.width(), PXH_START, GxEPD_WHITE);

  display.setCursor(0, 4);
  display.print(messageBuffer);
}

#define MARKER_OFFSET 4

void renderBlock(uint8_t slot, bool isNow, uint16_t halfhour, const int bgColor) {
  const int fgColor = bgColor == GxEPD_WHITE ? GxEPD_BLACK : GxEPD_WHITE;
  const int width = display.width() / 2;
  const int xBasePos = (slot % 2) * width;
  const int yBasePos = PXH_START + (slot / 2) * PXH_PER_HOUR;

  display.setTextSize(1);
  display.setTextColor(fgColor);
  display.fillRect(xBasePos, yBasePos, width, PXH_PER_HOUR, bgColor);
  display.drawFastHLine(xBasePos, yBasePos, width, fgColor);

  display.setCursor(xBasePos + 4, yBasePos + 5);
  display.printf("%02d:%02d", halfhour / 2, (halfhour % 2) * 30);
  if (isNow) {
#if 0
    display.printf(" <<<");
#else
    const int xEndPos = xBasePos + width;
    display.fillTriangle(xEndPos                - MARKER_OFFSET, yBasePos                + MARKER_OFFSET,
                         xEndPos                - MARKER_OFFSET, yBasePos + PXH_PER_HOUR - MARKER_OFFSET,
                         xEndPos - PXH_PER_HOUR + MARKER_OFFSET, yBasePos + PXH_PER_HOUR - MARKER_OFFSET,
                         fgColor);
#endif
  }
}

void renderBlocks() {
  int halfhourNow = (timeinfo.tm_wday * 48) + (timeinfo.tm_hour * 2) + (timeinfo.tm_min < 30 ? 0 : 1);

#if defined(DISPLAY_HOUR_START)
  int day = timeinfo.tm_wday;
  if (timeinfo.tm_hour >= (DISPLAY_HOUR_START + HOURS_TO_SHOW)) {
    day = (day + 1) % 7;
  }
  int schedBase = (day * 48) + DISPLAY_HOUR_START * 2;
#else
  int schedBase = (timeinfo.tm_wday * 48) + (timeinfo.tm_hour * 2) - 2;
#endif

  for (uint8_t slot = 0; slot < (HOURS_TO_SHOW * 2); ++slot) {
    const int halfhour = schedBase + slot;
    if (halfhour >= 0 && halfhour < LWIP_ARRAYSIZE(schedule)) {
      renderBlock(slot, halfhour == halfhourNow, halfhour % 48, BusyStateToColor(schedule[halfhour]));
    }
  }
}

void renderDisplay() {
  display.fillScreen(GxEPD_WHITE);

  renderStatus();
  renderBlocks();

  display.display();
}